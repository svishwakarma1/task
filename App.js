import React,{ useEffect, Component } from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Provider, useDispatch, connect } from 'react-redux';

import ListScreen from './Src/Screens/ListScreen';
import DetailScreen from './Src/Screens/DetailScreen';
import { getList } from './Src/Redux/Actions/ContactAction';


const Stack = createStackNavigator();

// function App() {
    
//   useEffect(() => {
//     get_list()
//   }); 

//   return (
//     <NavigationContainer>
//       <Stack.Navigator screenOptions={{headerShown: true}} initialRouteName="ListScreen">
        
//         <Stack.Screen name="ListScreen" component={ListScreen} />
//         <Stack.Screen name="DetailScreen" component={DetailScreen} />

//       </Stack.Navigator>
//     </NavigationContainer>
//   );
// }

class App extends Component {

  componentDidMount(){
    this.props.get_list();
  }

  render(){
    return (
      <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown: true}} initialRouteName="ListScreen">
          
          <Stack.Screen name="ListScreen" component={ListScreen} options={{ title: 'CONTACT LIST' }}/>
          <Stack.Screen name="DetailScreen" component={DetailScreen} options={({ route }) => ({ title: 'DETAIL',headerBackTitle:"" })} />

        </Stack.Navigator>
      </NavigationContainer>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    get_list:() => {dispatch(getList())}
  }
}

export default connect(null,mapDispatchToProps)(App);



