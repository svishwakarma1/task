import axios from 'axios';
import { CONTACT } from './../Constant';

export const getList = () => {
    return (dispatch) => {
    	axios('https://jsonplaceholder.typicode.com/users')
	    .then((res) => {
	    	if(res.data.length!==0){
	    		let dataObj = { contacts: res.data, loading:false }	    	
		    	dispatch({
		        	type:CONTACT.GET_LIST,
		        	payload:dataObj
		    	})
	    	}
	    }).catch((error) => {
	    	let dataObj = { contacts: [], loading:false }	    	
	    	dispatch({
	        	type:CONTACT.GET_LIST,
	        	payload:dataObj
	    	})
	    })
    }
}


export const updateContactList = (data) => {
    return {
        type:CONTACT.UPDATE_LIST,
        payload:data
    }
}
