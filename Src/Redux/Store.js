import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';


import contactReducer from './Reducer/ContactReducer';

const rootReducer = combineReducers({
    contact: contactReducer,
});
  
const configureStore = () => {
    return createStore(rootReducer, compose(applyMiddleware(thunk)));
}
  
export default configureStore;

