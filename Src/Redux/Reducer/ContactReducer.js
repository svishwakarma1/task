import { CONTACT } from './../Constant';

const initialState = {
    list: [],
    loading:true
}

const ContactReducer = (state = initialState, action) => {
    switch(action.type){
        
        case CONTACT.GET_LIST:
            return{
                ...state,
                list: action.payload.contacts,
                loading:action.payload.loading
            }
        
        case CONTACT.UPDATE_LIST:
            return{
                ...state,
                list: action.payload
            }

        default:
            return state;
    }
    return state;
}

export default ContactReducer;