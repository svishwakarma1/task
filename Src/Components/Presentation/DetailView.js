import React from 'react';
import { View, Text, Image } from 'react-native';
import { RFValue } from "react-native-responsive-fontsize";

import TextView from './TextView';
import Colors from './../../assets/Constant/Colors';

const DetailView = (props) => {

	let { text, icon, style } = props;

	return (
		<View style={[{flexDirection:'row',alignItems:'center'},style]}>
			<Image source={icon}
				style={{height:RFValue(20),width:RFValue(20),resizeMode:'contain',marginHorizontal:RFValue(20),opacity:0.6}}
			/>

			<TextView
				style={{flex:1,paddingRight:RFValue(10),paddingVertical:RFValue(5),opacity:0.6}}
				numberOfLines={1}
				fontSize={12}
				fontFamily="semibold"
			>
				{text}
			</TextView>
		</View>
	)
}

export default DetailView;