import React from 'react';
import { Text, View, TouchableOpacity, Dimensions } from 'react-native';
import { RFValue } from "react-native-responsive-fontsize";

import TextView from './TextView';
import Colors from './../../assets/Constant/Colors';

const { width, height } = Dimensions.get('window');

const ButtonView = (props) => {
	let { onPress, children, textStyle, style } = props;
	return (
		<TouchableOpacity
			onPress={() => {
				if(onPress)
					onPress()
			}}
			activeOpacity={1}
			style={[{height:RFValue(40),width:width * 0.8,alignItems:'center',justifyContent:'center',borderRadius:13,borderColor:Colors.borderColor,borderWidth:1},style]}
		>
			<TextView
				style={textStyle}
				fontSize={14}
			>
				{ children }
			</TextView>
		</TouchableOpacity>
	)
}
export default ButtonView;