import React from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet, ImageBackground } from 'react-native';
import { RFValue } from "react-native-responsive-fontsize";

import TextView from './TextView';
import Colors from './../../assets/Constant/Colors';

const CardView = (props) => {
	let { detail, onPress, style, uniquekey } = props;
	return(
		<TouchableOpacity style={[styles.cardContainer,style]}
			activeOpacity={0.6}
		  	onPress={() => {
		  		if(onPress)
		  			onPress(detail)
		  	}}
		  	key={uniquekey}
		>
			<View style={{paddingLeft:RFValue(10)}}>
				<ImageBackground source={require('./../../assets/Images/Icons/user.png')}
					style={styles.imageStyle}
				>
					<Image 
						source={{uri:"https://via.placeholder.com/150/810b14"}}
						style={styles.imageStyle}
					/>
				</ImageBackground>
			</View>
			<View style={styles.contentContainer}>
				<TextView
					numberOfLines={2}
					labelColor={Colors.textColor}
					style={{textTransform:'uppercase'}}
				>
					{detail.name || "Untitle"}
				</TextView>
				<View style={styles.contentDetailContainer}>
					<TextView
						style={{}}
						numberOfLines={1}
						fontSize={11}
						fontFamily="semibold"
						style={{textTransform:'uppercase'}}
					>
						8898046499
					</TextView>
					<TextView>

					</TextView>
				</View>
			</View>
		</TouchableOpacity>
	)
}

const styles = StyleSheet.create({
	cardContainer:{
		flexDirection:'row',
		//borderBottomWidth:1,
		//borderBottomColor:Colors.borderColor
	},
	imageStyle:{
		height:RFValue(50),
		width:RFValue(50),
		borderRadius:25,
		resizeMode:'cover'
	},
	contentContainer:{
		flex:1,
		justifyContent:'space-between',
		paddingVertical:RFValue(10),
		paddingHorizontal:RFValue(10)
	},
	contentDetailContainer:{
		flexDirection:'row',
		alignItems:'center',
		justifyContent:'space-between'
	}
})

export default CardView;


