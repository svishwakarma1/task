import React from 'react';
import { View, Text } from 'react-native';
import { RFValue } from "react-native-responsive-fontsize";

import Colors from './../../assets/Constant/Colors';
import { getFontFamily } from './../../assets/Constant/Helper';

const TextView = (props) => {
	let { children, style, labelColor, fontSize, numberOfLines, fontFamily } = props;

	labelColor = labelColor || Colors.textColor;
	fontSize = RFValue(fontSize) || RFValue(16);
	numberOfLines = numberOfLines || 1000;
	fontFamily = getFontFamily(fontFamily) || getFontFamily('medium')

	return (
		<Text 
			style={[{fontSize:fontSize,color:labelColor,fontFamily:fontFamily},style]}
			numberOfLines={numberOfLines}
		>
			{ children }
		</Text>
	)
}

export default TextView;