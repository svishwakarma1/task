import React,{ Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, Image, StyleSheet } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { RFValue } from "react-native-responsive-fontsize";

import TextView from './../../Components/Presentation/TextView';
import Colors from './../../assets/Constant/Colors';

class FrameLayout extends Component {

	constructor(props){
		super(props);
		this.state = {
			networkConnected:true,
			count:0
		}
	}

	checkConnectivity = () => {
      	this._subscription = NetInfo.addEventListener(
      		this._handleConnectivityChange,
    	)
	}

	componentDidMount(){
		this.checkConnectivity();
	}

	componentWillUnmount(){
		this._subscription();
	}

	_handleConnectivityChange = (state) => {
	    this.setState({
	      networkConnected: state.isConnected,
	    });
  	}
	
	render(){

		let { children, style } = this.props;
		let { networkConnected } = this.state;

		return (
			<SafeAreaView style={[{flex:1,backgroundColor:Colors.white},style]}>
				{
					!networkConnected &&
					<TouchableOpacity onPress={() => this.checkConnectivity()}
						style={styles.networkContainer}
						activeOpacity={0.5}
					>
						<Image 
							source={require('./../../assets/Images/Icons/network.png')}
							style={styles.networkImage}
						/>
						<TextView
							labelColor={Colors.primary}
						>
							Your Network is disconnected
						</TextView>
						<TextView
							fontFamily="regular"
							fontSize={14}
						>
							Click here to check again
						</TextView>
					</TouchableOpacity>
				}


				{ networkConnected && children }

				<SafeAreaView />
			</SafeAreaView>
		)
	}
}

const styles = StyleSheet.create({
	networkContainer:{
		flex:1,
		alignItems:'center',
		justifyContent:'center'
	},
	networkImage:{
		height:RFValue(30),
		width:RFValue(30),
		resizeMode:'contain',
		marginBottom:RFValue(10)
	}
})

export default FrameLayout;