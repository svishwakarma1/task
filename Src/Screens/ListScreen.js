import React, { Component } from 'react';
import { View, Text, FlatList, ActivityIndicator, StyleSheet, Image, Dimensions, SafeAreaView } from 'react-native';
import { RFValue } from "react-native-responsive-fontsize";
import { connect } from 'react-redux';

import FrameLayout from './../Components/Layouts/FrameLayout';
import Colors from './../assets/Constant/Colors';
import CardView from './../Components/Presentation/CardView';
import TextView from './../Components/Presentation/TextView';

const  { width, height } = Dimensions.get('window');
class ListScreen extends Component {

	constructor(props){
		super(props);
		this.state = {

		}
	}
	
	onPress = (item) => {
		let { navigation } = this.props;
		navigation.push('DetailScreen',{id:item.id})
	}

	renderVenue = ({item,index}) => {
		return (
			<CardView
				uniquekey={index}
				detail={item}
				style={{paddingVertical:RFValue(20)}}
				onPress={(item) => this.onPress(item)}
			/>
		)
	}

	showEmptyListView = () => {
		return (
			<View style={{height:height * 0.80,alignItems:'center',justifyContent:'center'}}>
				<Image 
					source={require('./../assets/Images/Icons/empty.png')}
					style={{height:RFValue(50),width:RFValue(50),resizeMode:'contain',opacity:0.6}}
				/>
				<TextView
					style={{opacity:0.6}}
				>
					No Data
				</TextView>
			</View>
		)
	}

	render(){
		const onLoading = this.props.loading;

		return (
			<FrameLayout
				style={{}}
			>
				{
					onLoading &&
					<View style={styles.loaderContainer}>
						<ActivityIndicator size="large" color={Colors.primary}/>
						<TextView
							fontSize={14}
							labelColor={Colors.primary}
							style={{marginTop:RFValue(5)}}
						>
							Please wait
						</TextView>
					</View>
				}

				{
					!onLoading &&
					<FlatList
						data={this.props.contactList}
						renderItem={this.renderVenue}
	                  	keyExtractor={item => item.id}
			            onEndThreshold={0}
			            ListEmptyComponent={this.showEmptyListView}
			            showsVerticalScrollIndicator={false}
					/>
				}
			</FrameLayout>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		contactList:state.contact.list,
		loading:state.contact.loading
	}
}

const styles = StyleSheet.create({
	loaderContainer:{
		flex:1,
		alignItems:'center',
		justifyContent:'center'
	}
});


export default connect(mapStateToProps,null)(ListScreen);


