import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, Dimensions, StyleSheet, ActivityIndicator, Alert, ImageBackground } from 'react-native';
import { RFValue } from "react-native-responsive-fontsize";
import { connect } from 'react-redux';
import axios from 'axios';

import FrameLayout from './../Components/Layouts/FrameLayout';
import CardView from './../Components/Presentation/CardView';
import TextView from './../Components/Presentation/TextView';
import ButtonView from './../Components/Presentation/ButtonView';
import DetailView from './../Components/Presentation/DetailView';
import Colors from './../assets/Constant/Colors';
import { updateContactList } from './../Redux/Actions/ContactAction';

const { width, height } = Dimensions.get('window');

class DetailScreen extends Component {

	constructor(props){
		super(props);

		let id = null;
		if(this.props.route.params!==undefined){
			id = this.props.route.params.id
		}

		this.state = {
			id:id,
			data:null,
			image:null,
			isLoading:false
		}
	}

	componentDidMount(){
		this.getDetail();
	}

	getDetail = () => {
		let { id } = this.state;
		if(id !== null){
			this.setState({
				isLoading:true
			}, async () => {
				let response = await axios("https://jsonplaceholder.typicode.com/users/" + id);
				if(response.data!==undefined){
					this.setState({data:response.data},() => {
						this.getImage();
					})
				}

				this.setState({isLoading:false})
			})
		}
	}

	getImage = async () => {
		let { id } = this.state;
		if(id !== null){
			let response = await axios("https://jsonplaceholder.typicode.com/photos/" + id);
			if(response.data!==undefined){
				this.setState({image:response.data.thumbnailUrl})
			}
		}
	}

	onDeletContact = () => {
		Alert.alert(
	      "Confirmation",
	      "Are you sure want to delete?",
	      [
	        {
	          text: "Cancel",
	          style: "cancel"
	        },
	        { text: "OK", onPress: () => this.deleteContact() }
	      ]
	    );
	}

	deleteContact = () => {
		let { data } = this.state;
		let { contactList, navigation } = this.props;

		let updatedData = contactList.filter((x) => x.id !== data.id);
		this.props.updateList(updatedData);
		navigation.goBack();
	}

	render(){
		let {  data, image, isLoading } = this.state; 
		return(
			<FrameLayout
				style={{}}
			>	
				{
					isLoading &&
					<View style={styles.loaderContainer}>
						<ActivityIndicator size="large" color={Colors.primary}/>
						<TextView
							fontSize={14}
							labelColor={Colors.primary}
							style={{marginTop:RFValue(5)}}
						>
							Please wait
						</TextView>
					</View>
				}

				{
					(data!=null && !isLoading) && 
					<View style={styles.container}>
						<ImageBackground source={require('./../assets/Images/Icons/user.png')}
							style={styles.contactImage}
						>
							<Image 
								source={{uri:image}}
								style={styles.contactImage}
							/>
						</ImageBackground>

						<View style={{bottom:RFValue(50)}}>
							<TextView
								style={styles.nameStyle}
								fontSize={20}
							>
								{data.name}
							</TextView>

							<TextView
								style={styles.subNameStyle}
								numberOfLines={1}
								fontSize={14}
							>
								{data.username}
							</TextView>
						</View>

						<View style={styles.detailCard}>
							
							<DetailView 
								text={data.phone}
								icon={require('./../assets/Images/Icons/call.png')}
							/>

							<DetailView 
								text={data.email}
								icon={require('./../assets/Images/Icons/mail.png')}
								style={{marginTop:RFValue(20)}}
							/>

							<DetailView 
								text={data.address.street +  data.address.suite + data.address.city}
								icon={require('./../assets/Images/Icons/address.png')}
								style={{marginTop:RFValue(20)}}
							/>

							<DetailView 
								text={data.website}
								icon={require('./../assets/Images/Icons/web.png')}
								style={{marginTop:RFValue(20)}}
							/>

						</View>

						<ButtonView
							onPress={this.onDeletContact}
							style={{position:'absolute',bottom:0}}
						>
							DELETE
						</ButtonView>
					</View>
				}
			</FrameLayout>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		contactList:state.contact.list
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		updateList:(data) => {
			dispatch(updateContactList(data))
		}
	}
}

const styles = StyleSheet.create({
	container:{
		flex:1,
		alignItems:'center',
		justifyContent:'space-evenly'
	},
	contactImage:{
		height:RFValue(150),
		width:RFValue(150),
		resizeMode:'contain',
		borderRadius:75
	},
	loaderContainer:{
		flex:1,
		alignItems:'center',
		justifyContent:'center'
	},
	nameStyle:{
		textAlign:'center'
	},
	subNameStyle:{
		textAlign:'center',
		paddingHorizontal:RFValue(50),
		paddingVertical:RFValue(5),
		opacity:0.6
	},
	detailCard:{
		width:width * 0.8,
		alignSelf:'center',
		paddingVertical:RFValue(20),
		borderRadius:20,
		borderColor:Colors.borderColor,
		borderWidth:1
	}

});


export default connect(mapStateToProps,mapDispatchToProps)(DetailScreen);

