const Colors = {

	primary:"#000000",
	secondary:"",
	textColor:"#000000",
	borderColor:"#c2c4c3",
	white:"#ffffff"
}

export default Colors;