export const getFontFamily = (fontFamily) => {

	if(fontFamily == 'light'){
		return 'Poppins-Light';
	}else if(fontFamily == 'medium'){
		return 'Poppins-Medium';
	}else if(fontFamily == 'semibold'){
		return 'Poppins-SemiBold';
	}else if(fontFamily == 'bold'){
		return 'Poppins-Bold';
	}else if(fontFamily == 'regular'){
		return 'Poppins-Regular';
	}else{
		return 'Poppins-Medium';
	}
	
}