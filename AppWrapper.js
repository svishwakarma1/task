import React,{ useEffect } from 'react';
import { Provider, useDispatch } from 'react-redux';

import configureStore from './Src/Redux/Store';
import App from './App';

function AppWrapper() {
  const store = configureStore();
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
}

export default AppWrapper;



